﻿using System;
using System.Text;
using System.Threading.Tasks;

namespace RPGTest2
{
    public class Actor
    {
        public int Health { get; set; }
        public int Defense { get; set; }
        public int Speed { get; set; }
        public int Location { get; set; }
        public string Name { get; set; }
        public string Class { get; set; }
        public string Armor { get; set; }
        public string Weapon { get; set; }
        public int Attack { get; set; }
        public bool HasWeapon { get; set; }
        public bool HasArmor { get; set; }
        public int HeldMoney { get; set; }
        public bool InBattle { get; set; }
        public string BattlingWith { get; set; }
        public bool TakenTurn { get; set; }
    }
}
