﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGTest2
{
    public class Item
    {
        public string ItemName { get; set; }
        public int ItemDefense { get; set; }
        public int ItemAttack { get; set; }
        public int ItemQuanity { get; set; }
        public int ItemValue { get; set; }

        public Item(string Name, int Defense, int Attack, int Quanity, int Value)
        {
            ItemName = Name;
            ItemDefense = Defense;
            ItemAttack = Attack;
            ItemQuanity = Quanity;
            ItemValue = Value;
        }

    }
}
