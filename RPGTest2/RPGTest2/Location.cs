﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGTest2
{
    class Location
    {
        public string LocationName { get; set; }
        public int LocationID { get; set; }
        public Location(string Name, int ID)
        {
            LocationName = Name;
            LocationID = ID;
        }
    }
}
