﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGTest2
{
    class Game
    {
        //"Enemy" exists here for when an actor becomes an enemy(as in, you're battling them)
        //This cuts down on how many times I have to re-write battle methods.
        public static Actor Enemy;
        public static Actor Player;
        public static Actor WildDog;
        public static Actor Shopkeeper;
        public static Item chain = new Item("Chainmail", 3, 0, 1, 15);
        public static Item robe = new Item("Robe", 1, 0, 1, 10);
        public static Item dagger = new Item("Dagger", 0, 5, 1, 10);
        public static Item staff = new Item("Staff", 0, 3, 1, 5);
        public static Item longsword = new Item("Longsword", 0, 10, 1, 20);
        public static Item sword = new Item("Sword", 0, 7, 1, 15);
        public static Money Grand = new Money();
        public static Location campsite = new Location("Campsite", 1);
        public Location church = new Location("Church", 2);
        public Location forest = new Location("Forest", 3);
        public string response;
        public static Command Basic = new Command();
        public List<string> Classes = new List<string>
        {
            "knight",
            "mage",
            "thief",
            "cleric"
        };
        public List<string> Commands = new List<string>
        {
            "north",
            "south",
            "east",
            "west",
            "talk",
            "take",
            "attack",
            "defend",
            "cast",
            "shop",
            "move"
        };
        public static List<Item> ArmorList = new List<Item>();
        public static List<Item> WeaponList = new List<Item>();
        static void Main(string[] args)
        {
            Game Game = new Game();
            Game.Run();
        }

        public void Run()
        {
            PopulateLists();
            PopulateEnemies();
            Player = new Actor();
            Console.WriteLine("A voice beckons from the shadows...");
            Console.WriteLine("\"Hello, traveler. What is your name?\"");
            Player.Name = Console.ReadLine();
            Console.WriteLine("\"Your name is " + Player.Name + ".\"");
            Console.WriteLine("Who were you in your past life?");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Choose a class.");
            Console.ResetColor();
            ClassChoice();
            Console.WriteLine("\"Your class is " + Player.Class + ".\"");
            Player.Health = 20;
            Player.Speed = 5;
            ItemDeterminer();
            GameThread();
        }
        public void PopulateLists()
        {
            Grand.NameOfCoin = "Grands";
            Grand.ValueOfCoin = 1;
            ArmorList.Add(chain);
            ArmorList.Add(robe);
            WeaponList.Add(staff);
            WeaponList.Add(dagger);
            WeaponList.Add(sword);
            WeaponList.Add(longsword);
        }
        public void PopulateEnemies()
        {
            WildDog = new Actor();
            WildDog.Name = "Wild Dog";
            WildDog.Attack = 5;
            WildDog.Health = 10;
            WildDog.Defense = 1;
            WildDog.Speed = 5;
            WildDog.HasWeapon = true;
            WildDog.Weapon = "Bite";
            WildDog.Location = 3;
        }
        public void PopulateNPCS()
        {
            Shopkeeper = new Actor();

        }
        public string ClassChoice()
        {
            var Test = true;
            while (Test)
            {
                Player.Class = Console.ReadLine().ToLower();
                if (Classes.Contains(Player.Class))
                {
                    Test = false;
                }
                else
                {
                    Console.WriteLine("I'm sorry, that profession doesn't exist anymore.");
                }
            }
            return Player.Class;
        }
        public void ItemDeterminer()
        {
            //Random seed based on time
            Random rnd = new Random();
            //Armor
            int a = rnd.Next(0, 1);
            //Wepons
            int w = rnd.Next(0, 3);
            //Money
            int m = rnd.Next(1, 20);
            //Knight
            if (Player.Class == Classes[0])
            {
                //Determine Armor & Weapon randomly

                if (ArmorList[a] == ArmorList[0])
                {
                    Player.Armor = chain.ItemName;
                    Player.Defense = chain.ItemDefense;
                }
                if (ArmorList[a] == ArmorList[1])
                {
                    Player.Armor = robe.ItemName;
                    Player.Defense = robe.ItemDefense;
                }
                if (WeaponList[w] == WeaponList[0])
                {
                    Player.Weapon = staff.ItemName;
                    Player.Attack = staff.ItemAttack;
                }
                if (WeaponList[w] == WeaponList[1])
                {
                    Player.Weapon = dagger.ItemName;
                    Player.Attack = dagger.ItemAttack;
                }
                if (WeaponList[w] == WeaponList[2])
                {
                    Player.Weapon = sword.ItemName;
                    Player.Attack = sword.ItemAttack;
                }
                if (WeaponList[w] == WeaponList[3])
                {
                    Player.Weapon = longsword.ItemName;
                    Player.Attack = longsword.ItemAttack;
                }
            }
            //Mage
            if (Player.Class == Classes[1])
            {
                if (ArmorList[a] == ArmorList[0])
                {
                    Player.Armor = chain.ItemName;
                    Player.Defense = chain.ItemDefense;
                }
                if (ArmorList[a] == ArmorList[1])
                {
                    Player.Armor = robe.ItemName;
                    Player.Defense = robe.ItemDefense;
                }
                if (WeaponList[w] == WeaponList[0])
                {
                    Player.Weapon = staff.ItemName;
                    Player.Attack = staff.ItemAttack;
                }
                if (WeaponList[w] == WeaponList[1])
                {
                    Player.Weapon = dagger.ItemName;
                    Player.Attack = dagger.ItemAttack;
                }
                if (WeaponList[w] == WeaponList[2])
                {
                    Player.Weapon = sword.ItemName;
                    Player.Attack = sword.ItemAttack;
                }
                if (WeaponList[w] == WeaponList[3])
                {
                    Player.Weapon = longsword.ItemName;
                    Player.Attack = longsword.ItemAttack;
                }
            }
            //Theif
            if (Player.Class == Classes[2])
            {
                if (ArmorList[a] == ArmorList[0])
                {
                    Player.Armor = chain.ItemName;
                    Player.Defense = chain.ItemDefense;
                }
                if (ArmorList[a] == ArmorList[1])
                {
                    Player.Armor = robe.ItemName;
                    Player.Defense = robe.ItemDefense;
                }
                if (WeaponList[w] == WeaponList[0])
                {
                    Player.Weapon = staff.ItemName;
                    Player.Attack = staff.ItemAttack;
                }
                if (WeaponList[w] == WeaponList[1])
                {
                    Player.Weapon = dagger.ItemName;
                    Player.Attack = dagger.ItemAttack;
                }
                if (WeaponList[w] == WeaponList[2])
                {
                    Player.Weapon = sword.ItemName;
                    Player.Attack = sword.ItemAttack;
                }
                if (WeaponList[w] == WeaponList[3])
                {
                    Player.Weapon = longsword.ItemName;
                    Player.Attack = longsword.ItemAttack;
                }

            }
            //Cleric
            if (Player.Class == Classes[3])
            {
                if (ArmorList[a] == ArmorList[0])
                {
                    Player.Armor = chain.ItemName;
                    Player.Defense = chain.ItemDefense;
                }
                if (ArmorList[a] == ArmorList[1])
                {
                    Player.Armor = robe.ItemName;
                    Player.Defense = robe.ItemDefense;
                }
                if (WeaponList[w] == WeaponList[0])
                {
                    Player.Weapon = staff.ItemName;
                    Player.Attack = staff.ItemAttack;
                }
                if (WeaponList[w] == WeaponList[1])
                {
                    Player.Weapon = dagger.ItemName;
                    Player.Attack = dagger.ItemAttack;
                }
                if (WeaponList[w] == WeaponList[2])
                {
                    Player.Weapon = sword.ItemName;
                    Player.Attack = sword.ItemAttack;
                }
                if (WeaponList[w] == WeaponList[3])
                {
                    Player.Weapon = longsword.ItemName;
                    Player.Attack = longsword.ItemAttack;
                }

            }
            Player.HeldMoney = m;
            Player.HasArmor = true;
            Player.HasWeapon = true;
            Console.WriteLine("Your armor is a set of " + Player.Armor + ".");
            Console.WriteLine("Your weapon is a " + Player.Weapon + ".");
            Console.WriteLine("You are given " + Player.HeldMoney + " Grands.");
            Console.Read();
        }
        public void GameThread()
        {
            Console.Clear();
            Console.WriteLine("The fire has long since dwindled away. You awaken, items at your side.");
            Player.Location = campsite.LocationID;
            Console.WriteLine("North of this campsite, there is a small village nearby. You can hear the sound of morning church bells giving way to the laughter of children.");
            Console.WriteLine("To the south is a dense forest. You don't know what lies within.");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("What would you like to do?");
            Console.ResetColor();
            Basic.GetInput();

        }
    }
}