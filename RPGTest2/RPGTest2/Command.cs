﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGTest2
{
    class Command : Game
    {
        public string GetInput()
        {
            var Test = true;

            while (Test)
            {
                response = Console.ReadLine().ToLower();
                if (Commands.Contains(response))
                {
                    Test = false;
                    ProcessInput(response);
                }
                else
                {
                    Console.WriteLine("I'm sorry, I do not understand.");
                }
            }
            return response;
        }

        public void ProcessInput(string response)
        {
            switch (response)
            {
                case "move":
                    MoveCommand();
                    break;
                case "north":
                    MoveCommand();
                    break;
                case "south":
                    MoveCommand();
                    break;
                case "east":
                    MoveCommand();
                    break;
                case "west":
                    MoveCommand();
                    break;
                case "talk":
                    TalkCommand();
                    break;
                case "take":
                    TakeCommand();
                    break;
                case "attack":
                    AttackCommand();
                    break;
                case "defend":
                    DefendCommand();
                    break;
                case "cast":
                    CastCommand();
                    break;
                case "shop":
                    ShopCommand();
                    break;
            }
        }
        public void MoveCommand()
        {
            if (response == "move")
            {
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("Where to?");
                Console.ResetColor();
                GetInput();
            }
            if (response == "north")
            {
                //Campsite
                if (Player.Location == 1)
                {
                    Player.Location = church.LocationID;
                    Console.WriteLine("You arrive in a small village, approaching a church. Children run past.");
                    Console.WriteLine("There are a few shops set up nearby. If you have the cash, you can shop there.");
                    Console.WriteLine("Or, you can talk with the shopkeepers for information.");
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine("What would you like to do?");
                    Console.ResetColor();
                    GetInput();
                }
                if (Player.Location == 3)
                {
                    Player.Location = campsite.LocationID;
                    Console.WriteLine("You return to the campsite. It seems safer.");
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine("What would you like to do?");
                    Console.ResetColor();
                    GetInput();
                }
            }
            if (response == "south")
            {
                //Campsite
                if (Player.Location == 1)
                {
                    Player.Location = forest.LocationID;
                    Console.WriteLine(Player.Weapon + " gripped in hand, you decide to venture deep into the forest.");
                    Console.WriteLine("Leaves crunch underfoot as you creep into the dense wood. You feel tense. Something is watching you.");
                    if (WildDog.Location == 3)
                    {
                        Console.WriteLine("[???]: T̡̢͖̟̩̹̯̱̞͊́̄̐̂̒͘͠Ř̨͕͙̖̼̈́̐̎͆͌͝A̡͙̺̗͇͕̞̩̎́͆͑̍͌̕͜͝V̲̥̱̙͉̣͑̍͊̍͐̅̚͘͢͟͡È̮͓̰̲̣̞̝͎̓͐͆̅͐̂̕͜L̯̘̝̝͐́̑̾͂̓̒̈́͐͢͞E̵̺͓̤̦̩̫͖̦̒́͒̒́̈́̏̾͜͝R̨͚̫̥̼͖̞̍̂̀̇͐̊̄̍̑͝ͅ.");
                        Console.WriteLine("A wild dog jumps out at you!");
                        Player.InBattle = true;
                        Player.BattlingWith = WildDog.Name;
                        Enemy = WildDog;
                        Battle();
                    }
                }
            }
        }
        public void TakeCommand()
        {

        }
        public void TalkCommand()
        {

        }
        public void AttackCommand()
        {
            if (Player.InBattle == true)
            {
                int damage;
                damage = Player.Attack - Enemy.Defense;
                Console.WriteLine("You lunge at the dog!");
                Enemy.Health = Enemy.Health - damage;
                Console.WriteLine("You deal " + damage.ToString() + " damage to the " + Enemy.Name + "!");
                return;
            }


        }
        public void DefendCommand()
        {
            if (Player.InBattle == true)
            {
                int playerdamage;
                playerdamage = Enemy.Attack - Player.Defense;
                Console.WriteLine("You defend against the " + Enemy.Name + "!");
                Player.Health = Player.Health - playerdamage;
                Console.WriteLine("You take " + playerdamage.ToString() + " damage.");
                return;
            }
        }
        public void CastCommand()
        {

        }
        public void ShopCommand()
        {
            if (Player.Location == 2)
            {
                Console.WriteLine("The shopkeeper shows you his wares.");
                Console.WriteLine("---For Sale---");
                Console.WriteLine(sword.ItemName + "-" + sword.ItemValue);
                Console.WriteLine(robe.ItemName + "-" + robe.ItemValue);
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("What would you like to do?");
                Console.ResetColor();
                GetInput();


            }
        }
        public void Battle()
        {
            while (Player.InBattle == true)
            {
                while (Enemy.Health > 0 && Player.Health > 0)
                {
                    Console.WriteLine("You are engaged by the " + Enemy.Name + "!");
                    if (Player.TakenTurn == false)
                    {
                        Console.ForegroundColor = ConsoleColor.Green;
                        Console.WriteLine("What do you want to do?");
                        Console.ResetColor();
                        GetInput();
                        Player.TakenTurn = true;
                    }
                    if (Player.TakenTurn == true && Enemy.Health > 0)
                    {
                        Console.WriteLine("The " + Enemy.Name + " attacks!");
                        int playerdamage;
                        playerdamage = Enemy.Attack - Player.Defense;
                        Player.Health = Player.Health - playerdamage;
                        Console.WriteLine("You take " + playerdamage.ToString() + " damage.");
                        Player.TakenTurn = false;
                    }
                }
                if (Enemy.Health <= 0)
                {
                    Console.WriteLine("You defeated the " + Enemy.Name + "!");
                    Console.WriteLine("You got: ");
                    Random rnd = new Random();
                    int i = rnd.Next(1, 10);
                    Console.WriteLine(i.ToString() + " Grands.");
                    Player.HeldMoney = Player.HeldMoney + i;
                    Player.InBattle = false;
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine("What do you want to do?");
                    Console.ResetColor();
                    GetInput();
                }
                if (Player.Health <= 0)
                {
                    Console.WriteLine("You were slain by the " + Enemy.Name + ".");
                    Console.WriteLine("Press enter to try again.");
                    Console.ReadKey();
                    Player.InBattle = false;
                    Console.Clear();
                    Run();
                }

            }
        }

    }
}

